# Space Game
A simple space invaders clone. Developed as a simple game to demo features of the avEngine.

![Screenshot](Screenshot.png "Screenshot")

## Building
Assets for this project need to be built before they can be used.
This project uses the avEngine asset pipeline, which is based on docker.
Docker must be installed to be able to build this project.

```bash
./resBuild.sh
```

This script will produce an output directory named 'build', where the built assets are stored.

## Running
Once built this project can be run like any other avEngine project
```bash
./av ~/space-game/avSetup.cfg
```