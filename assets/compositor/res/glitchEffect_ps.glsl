#version ogre_glsl_ver_330

vulkan_layout( location = 0 )
out vec4 fragColour;

vulkan_layout( location = 0 )
in block
{
	vec2 uv0;
} inPs;

vulkan_layout( ogre_t0 ) uniform texture2D Image;

vulkan( layout( ogre_s0 ) uniform sampler samplerState );

vulkan( layout( ogre_P0 ) uniform Params { )
	uniform float distortionFreq;
	uniform float distortionScale;
	uniform float distortionRoll;
	uniform float interference;
	uniform float frameLimit;
	uniform float frameShape;
	uniform float frameSharpness;
	uniform float time_0_X;
	uniform float sin_time_0_X;
vulkan( }; )

float rng2(vec2 seed, float iTime)
{
    return fract(sin(dot(seed * floor(iTime * 12.), vec2(127.1,311.7))) * 43758.5453123);
}

float rng(float seed, float iTime)
{
    return rng2(vec2(seed, 1.0), iTime);
}

void main()
{
   vec2 uv = inPs.uv0;
   vec2 blockS = floor(uv * vec2(24., 9.));
   vec2 blockL = floor(uv * vec2(8., 4.));
   float iTime = time_0_X;

   float lineNoise = pow(rng2(blockS, iTime), 8.0) * pow(rng2(blockL, iTime), 3.0) - pow(rng(7.2341, iTime), 17.0) * 2.;

   vec4 col1 = texture(vkSampler2D(Image, samplerState), uv);
   vec4 col2 = texture(vkSampler2D(Image, samplerState), uv + vec2(lineNoise * 0.03 * rng(5.0, iTime), 0));
   vec4 col3 = texture(vkSampler2D(Image, samplerState), uv - vec2(lineNoise * 0.03 * rng(31.0, iTime), 0));

   fragColour = vec4(vec3(col1.x, col2.y, col3.z), 1.0);
}
