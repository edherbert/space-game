//https://www.shadertoy.com/view/Md2GDw
//Abandoned attempt at creating a similar effect.

#include <metal_stdlib>
using namespace metal;

struct PS_INPUT
{
	float2 uv0;
};

struct Params
{
	float distortionFreq;
	float distortionScale;
	float distortionRoll;
	float interference;
	float frameLimit;
	float frameShape;
	float frameSharpness;
	float time_0_X;
	float sin_time_0_X;
};

fragment float4 main_metal
(
	PS_INPUT inPs [[stage_in]],
	texture2d<float>	Image			[[texture(0)]],
	texture2d<float>	ImageSecond			[[texture(1)]],
	texture3d<float>	Noise			[[texture(2)]],
	sampler				samplerState	[[sampler(0)]],
	sampler				samplerStateRand[[sampler(1)]],
	sampler				samplerState3D	[[sampler(2)]],

	constant Params &p [[buffer(PARAMETER_SLOT)]]
)
{

   float2 iResolution(600, 900);
   float iTime = p.time_0_X;
   float2 uv = inPs.uv0;

   //float2 uv = inPs.uv0.xy / iResolution.xy;
   float2 block = floor(uv.xy / float2(16));
   float2 uv_noise = block / float2(64);
   uv_noise += floor(float2(iTime) * float2(1234.0, 3543.0)) / float2(64);

   float block_thresh = pow(fract(iTime * 1236.0453), 2.0) * 0.2;
   float line_thresh = pow(fract(iTime * 2236.0453), 3.0) * 0.7;

   float2 uv_r = uv, uv_g = uv, uv_b = uv;

   // glitch some blocks and lines
   if (ImageSecond.sample(samplerState, uv_noise).r < block_thresh ||
      ImageSecond.sample(samplerStateRand, float2(uv_noise.y, 0.0)).g < line_thresh) {

      float2 dist = (fract(uv_noise) - 0.5) * 0.3;
      uv_r += dist * 0.1;
      uv_g += dist * 0.2;
      uv_b += dist * 0.125;
   }

   float4 outCol(0, 0, 0, 1);
   outCol.r = Image.sample(samplerStateRand, uv_r).r;
   outCol.g = Image.sample(samplerStateRand, uv_g).g;
   outCol.b = Image.sample(samplerStateRand, uv_b).b;

   return outCol;

   //return float4(1, 0, 0, 1);
   //return image;
}
