enum MENUS{
    main,
    help,
    settings
};

::GameStateMenu <- class{

    mMainMenuWindow_ = null;
    mSettingsMenuWindow_ = null;
    mHelpMenuWindow_ = null;

    constructor(){

    }

    function createBackButton(window){
        local backButton = window.createButton();
        backButton.setText("back");
        backButton.setSize(buttonSize);
        backButton.setDefaultFontSize(backButton.getDefaultFontSize() * 1.5);
        backButton.setPosition(_window.getWidth() / 2 - buttonSize.x / 2, _window.getHeight() - buttonSize.y - 30);
        backButton.attachListener(function(widget, action){
            if(action != 2) return;
            switchToWindow(MENUS.main);
        }, this);
    }

    function createLogo(window, panelWidth){
        local datablockPanel = window.createPanel();
        //Get the aspect ratio of the logo.
        //TODO add a method of getting the width and height of these.
        local aspect = 102.0 / 314.0;
        datablockPanel.setSize(panelWidth, panelWidth * aspect);
        datablockPanel.setDatablock("MenuLogo");

        return datablockPanel;
    }

    function createMainMenu(){
        mMainMenuWindow_ = _gui.createWindow();
        mMainMenuWindow_.setSize(_window.getWidth(), _window.getHeight());
        mMainMenuWindow_.setVisualsEnabled(false);

        local layoutLine = _gui.createLayoutLine();

        local buttonOptions = ["play", "help", "settings"];
        local buttonFunctions = [
            function(widget, action){
                if(action != 2) return;
                print("Playing");
                ::startState(::GameStatePlaying);
            },
            function(widget, action){
                if(action != 2) return;
                switchToWindow(MENUS.help);
            },
            function(widget, action){
                if(action != 2) return;
                switchToWindow(MENUS.settings);
            }
        ]

        local panelWidth = _window.getWidth() * 0.9;
        local item = createLogo(mMainMenuWindow_, panelWidth);
        layoutLine.addCell(item);

        foreach(i,c in buttonOptions){
            local button = mMainMenuWindow_.createButton();
            button.setDefaultFontSize(button.getDefaultFontSize() * 1.5);
            button.setText(c);
            button.setSize(buttonSize);
            button.attachListener(buttonFunctions[i], this);
            layoutLine.addCell(button);
        }

        layoutLine.setMarginForAllCells(0, 20);
        layoutLine.setPosition(_window.getWidth() / 2 - panelWidth / 2, 100);
        layoutLine.setGridLocationForAllCells(_GRID_LOCATION_CENTER);
        layoutLine.layout();
    }

    function createSettingsMenu(){
        mSettingsMenuWindow_ = _gui.createWindow();
        mSettingsMenuWindow_.setSize(_window.getWidth(), _window.getHeight());
        mSettingsMenuWindow_.setVisualsEnabled(false);

        local layoutLine = _gui.createLayoutLine();

        local label = mSettingsMenuWindow_.createLabel();
        label.setText("music:");
        layoutLine.addCell(label);
        local musicSlider = mSettingsMenuWindow_.createSlider();
        musicSlider.setSize(300, 32);
        layoutLine.addCell(musicSlider);

        label = mSettingsMenuWindow_.createLabel();
        label.setText("sound FX:");
        layoutLine.addCell(label);
        local soundEffectsSlider = mSettingsMenuWindow_.createSlider();
        soundEffectsSlider.setSize(300, 32);
        layoutLine.addCell(soundEffectsSlider);

        createBackButton(mSettingsMenuWindow_);

        layoutLine.layout();
    }

    function createHelpMenu(){
        mHelpMenuWindow_ = _gui.createWindow();
        mHelpMenuWindow_.setSize(_window.getWidth(), _window.getHeight());
        mHelpMenuWindow_.setVisualsEnabled(false);

        local layoutLine = _gui.createLayoutLine();

        local panelWidth = _window.getWidth() * 0.65;
        local item = createLogo(mHelpMenuWindow_, panelWidth);
        layoutLine.addCell(item);

        local text = @"A simple game for the avEngine.

This game was written to test the engine and its functions.
It is a re-write of an earlier 2D game I made in Javascript/HTML5.

Use the arrow keys to navigate the ship and space bar to fire.
On touch devices use the bottom portion of the screen to move the ship and the upper portion to fire.
";
        local label = mHelpMenuWindow_.createLabel();
        label.setTextHorizontalAlignment(_TEXT_ALIGN_CENTER);
        label.setText(text);
        label.setSize(_window.getWidth() - 60, _window.getHeight());
        layoutLine.addCell(label);

        layoutLine.setMarginForAllCells(0, 40);
        layoutLine.setPosition(0, 100);
        layoutLine.setGridLocationForAllCells(_GRID_LOCATION_CENTER);
        layoutLine.layout();

        createBackButton(mHelpMenuWindow_);
    }

    function start(){
        switchToWindow(MENUS.main);
    }

    function switchToWindow(window){
        destroyWindows();

        switch(window){
            case MENUS.settings:{
                createSettingsMenu();
                break;
            }
            case MENUS.help:{
                createHelpMenu();
                break;
            }
            case MENUS.main:{
                createMainMenu();
                break;
            }
        }
    }

    function destroyWindows(){
        if(mMainMenuWindow_ != null) _gui.destroy(mMainMenuWindow_);
        if(mSettingsMenuWindow_ != null) _gui.destroy(mSettingsMenuWindow_);
        if(mHelpMenuWindow_ != null) _gui.destroy(mHelpMenuWindow_);

        mMainMenuWindow_ = null;
        mSettingsMenuWindow_ = null;
        mHelpMenuWindow_ = null;
    }

    function end(){
        destroyWindows();
    }

    function update(){

    }

    function notifyTouchInputBegan(fingerId){}
    function notifyTouchInputEnded(fingerId){}
    function notifyTouchInputMotion(fingerId){}
};