const HEALTH_BAR_WIDTH = 500;
const HEALTH_BAR_HEIGHT = 30;

::HealthBar <- class {
    mWin_ = null;
    mBar_ = null;
    mMaxHealth_ = null;

    constructor(maxHealth){
        mMaxHealth_ = maxHealth;

        local win = _gui.createWindow();
        win.setPosition(100, 0);
        win.setSize(HEALTH_BAR_WIDTH + 100, HEALTH_BAR_HEIGHT + 10);
        win.setVisualsEnabled(false);
        win.setConsumeCursor(true);

        mWin_ = win;

        local bar = win.createPanel();
        bar.setSize(0, 0);
        bar.setDatablock("healthBarDiffuse");
        mBar_ = bar;

        setBarValue(mMaxHealth_);
    }

    function setBarValue(value){
        print("setting bar value " + value);
        mBar_.setSize((value.tofloat() / mMaxHealth_) * HEALTH_BAR_WIDTH, HEALTH_BAR_HEIGHT);
        mBar_.setHidden(value <= 0);
    }

    function destroy(){
        _gui.destroy(mWin_);
    }

};
