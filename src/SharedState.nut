::SharedState <- class{

    mStarNode_ = null;
    mStars_ = [];

    mStarDatablock_ = null;

    constructor(){
        mStarDatablock_ = _hlms.unlit.createDatablock("starDatablock", null, null, {"diffuse": "1 1 1"});

        createStars();
    }

    function update(){
        local movAmount = Vec3(0, 0.05, 0);
        foreach(s in mStars_){
            local pos = s.getPositionVec3();
            if(pos.y >= 40){
                pos.y = -20;
                s.setPosition(pos);
            }
            s.move(movAmount);
        }
    }

    function createStars(){
        mStarNode_ = _scene.getRootSceneNode().createChildSceneNode();

        local offset = Vec3(25, 60, 0);
        for(local i = 0; i < 150; i++){
            local newNode = mStarNode_.createChildSceneNode();
            newNode.setPosition((_random.randVec3() - 0.5) * offset);
            newNode.setScale(0.1, 0.1, 0.1);

            local item = _scene.createItem("plane")
            item.setDatablock(mStarDatablock_);
            newNode.attachObject(item);
            mStars_.append(newNode);
        }
    }
};