//Health
function component0Set(eid){
    local newVal = _component.user[Component.HEALTH].get(eid, 0);
    if(newVal <= 0){
        ::currentState.killEnemy(eid.getId());
    }
}