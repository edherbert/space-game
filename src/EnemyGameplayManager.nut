::EnemyGameplayManager <- class {

    enemies_ = null;

    constructor(enemiesTable){
        enemies_ = enemiesTable;
    }

    function createEnemyWave(startYOffset){
        const ENEMY_WAVE_WIDTH = 3;
        const ENEMY_WAVE_HEIGHT = 4;

        local genBigEnemy = _random.randInt(2) == 0;
        local bigEnemyX = _random.randInt(ENEMY_WAVE_WIDTH - 1);
        local bigEnemyY = _random.randInt(ENEMY_WAVE_HEIGHT - 1);
        print("Create big enemy: " + genBigEnemy);

        for(local y = 0; y < ENEMY_WAVE_HEIGHT; y++){
            for(local x = 0; x < ENEMY_WAVE_WIDTH; x++){
                if(genBigEnemy){
                    if(
                        (y == bigEnemyY && x == bigEnemyX + 1) ||
                        (y == bigEnemyY + 1 && x == bigEnemyX) ||
                        (y == bigEnemyY + 1 && x == bigEnemyX + 1)
                    ){
                        continue
                    }
                }
                local isBigEnemy = (genBigEnemy && y == bigEnemyY && x == bigEnemyX);
                local e = ::Enemy(SlotPosition(x * 4.8, startYOffset + 15 + y * 4, 0), isBigEnemy);
                enemies_[e.getId()] <- e;
            }
        }
    }

};