::Entity <- class{
    mMeshNode_ = null;
    mMesh_ = null;
    mEntity_ = null;
    mAnimNode_ = null;

    mOrientation_ = null;

    constructor(pos, meshName, createAnimNode=false){
        mEntity_ = _entity.createTracked(pos);
        assert(mEntity_.valid());
        if(meshName != null){
            mMeshNode_ = _scene.getRootSceneNode().createChildSceneNode();
            local parent = mMeshNode_;
            if(createAnimNode){
                mAnimNode_ = mMeshNode_.createChildSceneNode();
                parent = mAnimNode_;
            }
            mMesh_ = _scene.createItem(meshName);
            parent.attachObject(mMesh_);
            _component.sceneNode.add(mEntity_, mMeshNode_, true);
        }

        mOrientation_ = Quat();
        mOrientation_ = 0.0;

        setPosition(pos);
    }

    function move(amount){
        local slotPos = getPosition();
        setPosition(slotPos + amount);
    }

    function setPosition(pos){
        mEntity_.setPosition(pos);
    }

    function destroy(){
        _entity.destroy(mEntity_);
    }

    function getId(){
        return mEntity_.getId();
    }

    /**
    Return as a SlotPosition.
    */
    function getPosition(){
        return mEntity_.getPosition();
    }

    function setScale(vec){
        mMeshNode_.setScale(vec);
    }

    function updateRotation(){
        mOrientation_ += 0.01;
        local newOrientation = Quat(mOrientation_, Vec3(0, 1, 0));
        mMeshNode_.setOrientation(newOrientation);
    }
};

::Player <- class extends ::Entity{
    mOrientation_ = null;

    mParticleNode_ = null;
    mParticleSystem_ = null;

    constructor(pos){
        //Create the particle system.
        mParticleNode_ = _scene.getRootSceneNode().createChildSceneNode();
        mParticleSystem_ = _scene.createParticleSystem("particle_playerEngineFumes");
        mParticleNode_.setScale(0.1, 0.1, 0.1);
        mParticleNode_.attachObject(mParticleSystem_);

        base.constructor(pos, "PlayerShip.mesh");
        mMeshNode_.setScale(2, 2, 2);

        //Issues with the animation system as of right now.
        // local animationInfo = _animation.createAnimationInfo([mMeshNode_]);
        // local anim = _animation.createAnimation("ShipSpin", animationInfo);
        // _component.animation.add(mEntity_, anim);
    }

    function setPosition(pos){
        base.setPosition(pos);

        mParticleNode_.setPosition(pos.toVector3());
    }

    function destroy(){
        base.destroy();

        mParticleNode_.destroyNodeAndChildren();
    }
}

::Enemy <- class extends ::Entity{
    mAnim_ = 0.0;

    mLargeEnemyScale = Vec3(3, 3, 3);
    mRegularEnemyScale = Vec3(1.5, 1.5, 1.5);

    mEntityDatablocks = [
        "EnemyShipMaterialRed",
        "EnemyShipMaterialGreen",
        "EnemyShipMaterialMagenta",
    ];
    mEntityHealthTypes = [
        10,
        15,
        20
    ];

    constructor(pos, largeEnemy){
        base.constructor(pos, "EnemyShip.mesh", true);

        local receiverInfo = {
            "type" : _COLLISION_ENEMY
        };
        local shapeScaleSize = 1.1 * (largeEnemy ? 2 : 1);
        local shape = _physics.getCubeShape(shapeScaleSize, shapeScaleSize, shapeScaleSize);
        local damageReceiver = _physics.collision[0].createReceiver(receiverInfo, shape, pos);
        _physics.collision[0].addObject(damageReceiver);

        _component.collision.add(mEntity_, damageReceiver);

        _component.user[Component.HEALTH].add(mEntity_);
        _component.script.add(mEntity_, "res://src/EntityScript.nut");

        local enemyType = _random.randIndex(mEntityDatablocks);
        mMesh_.setDatablock(mEntityDatablocks[enemyType]);

        if(largeEnemy){
            setScale(mLargeEnemyScale);
            setHealth((mEntityHealthTypes[enemyType] * 1.5).tointeger());
        }else{
            setScale(mRegularEnemyScale);
            setHealth(mEntityHealthTypes[enemyType]);
        }
    }

    function updateAnimations(){
        updateRotation();

        //Make them shake after a drop in health.
        if(_component.user[Component.HEALTH].get(mEntity_, 0) <= 5){
            mAnim_ += 1;
            mAnimNode_.setPosition(sin(mAnim_) / 8, 0, 0);
        }
    }

    function setHealth(health){
        _component.user[Component.HEALTH].set(mEntity_, 0, health);
    }
}

::Projectile <- class extends ::Entity{
    mDamageSender_ = null;

    mParticleNode_ = null;
    mParticleSystem_ = null;

    constructor(pos){
        //Create the particle system.
        mParticleNode_ = _scene.getRootSceneNode().createChildSceneNode();
        mParticleSystem_ = _scene.createParticleSystem("particle_playerAttack");
        mParticleNode_.setScale(0.1, 0.1, 0.1);
        mParticleNode_.attachObject(mParticleSystem_);

        base.constructor(pos, "Missile.mesh");

        local senderInfo = {
            "func" : "projectile",
            "path" : "res://src/DamageCallback.nut"
            "id" : 0,
            "type" : _COLLISION_ENEMY,
            "event" : _COLLISION_ENTER
        };
        local shape = _physics.getCubeShape(1.1, 1.1, 1.1);

        mDamageSender_ = _physics.collision[0].createSender(senderInfo, shape, pos);
        _physics.collision[0].addObject(mDamageSender_);

        _component.collision.add(mEntity_, mDamageSender_);
    }

    function setPosition(pos){
        base.setPosition(pos);

        mParticleNode_.setPosition(pos.toVector3());
    }

    function destroy(){
        base.destroy();

        mParticleNode_.destroyNodeAndChildren();
    }
}