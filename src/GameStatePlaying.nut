::GameStatePlaying <- class {

    player_ = null;

    movementDir_ = false;
    movementCount_ = 0;

    mPauseButtonWindow_ = null;
    mPauseButton_ = null;
    mPauseWindow_ = null;

    mGameoverWindow_ = null;

    mHealthBar_ = null;
    mPlayerHealth_ = 30;

    mTotalKillCount_ = 0;
    mGameplaySpeed_ = 0.02;

    mTouchInputs_ = {};
    mTouchControllingPlayer = null;

    enemies_ = {};
    projectiles_ = {};
    mEnemyManager = null;

    constructor(){
        _doFile("res://src/EnemyGameplayManager.nut");
        mEnemyManager = EnemyGameplayManager(enemies_);
    }

    //Destroy but count as a player kill
    function killEnemy(id){
        destroyEnemy(id);

        mTotalKillCount_++;
        if(mTotalKillCount_ % 8 == 0){
            mGameplaySpeed_ += 0.005;
        }
    }

    function destroyEnemy(id){
        enemies_[id].destroy();
        enemies_.rawdelete(id);
    }
    function destroyProjectile(id){
        if(!(id in projectiles_)) return;
        projectiles_[id].destroy();
        projectiles_.rawdelete(id);
    }

    function createProjectile(){
        local pos = player_.getPosition();
        local e = ::Projectile(pos);
        print("Created with id " + e.getId());
        projectiles_[e.getId()] <- e;
    }

    function _createButtonList(win, layout, labels, functions){
        foreach(i,c in labels){
            local button = win.createButton();
            button.setDefaultFontSize(button.getDefaultFontSize() * 1.5);
            button.setKeyboardNavigable(false);
            button.setText(c);
            button.setSize(buttonSize);
            button.attachListenerForEvent(functions[i], 2, this);
            layout.addCell(button);
        }

        layout.setMarginForAllCells(0, 20);
        layout.setPosition(_window.getWidth() / 2 - buttonSize.x / 2, 100);
        layout.setGridLocationForAllCells(_GRID_LOCATION_CENTER);
    }

    function showPauseMenu(show){
        gameplayPaused = show;

        _state.setPauseState(show ? _PAUSE_PARTICLES : 0);

        if(show){
            local layout = _gui.createLayoutLine();

            mPauseWindow_ = _gui.createWindow();
            mPauseWindow_.setSize(_window.getWidth(), _window.getHeight());

            local labels = ["Back to game", "Main menu"];
            local buttonFunctions = [
                function(widget, action){ showPauseMenu(false); },
                function(widget, action){ startState(::GameStateMenu); },
            ]
            _createButtonList(mPauseWindow_, layout, labels, buttonFunctions);

            layout.layout();
        }else{
            assert(mPauseWindow_ != null);
            _gui.destroy(mPauseWindow_);
            mPauseWindow_ = null;
        }
    }

    function showGameoverMenu(show){
        if(mGameoverWindow_ != null) return;
        gameplayPaused = show;

        if(show){
            local layout = _gui.createLayoutLine();

            mGameoverWindow_ = _gui.createWindow();
            mGameoverWindow_.setSize(_window.getWidth(), _window.getHeight());

            local gameOverText = mGameoverWindow_.createLabel();
            gameOverText.setText("Game Over");
            gameOverText.setDefaultFontSize(gameOverText.getDefaultFontSize() * 2.5);
            gameOverText.setSize(gameOverText.getSize() * 2.5);
            layout.addCell(gameOverText);

            local buttonOptions = ["Play again", "Main menu"];
            local buttonFunctions = [
                function(widget, action){ startState(::GameStatePlaying); },
                function(widget, action){ startState(::GameStateMenu); },
            ]
            _createButtonList(mGameoverWindow_, layout, buttonOptions, buttonFunctions);

            layout.layout();

        }else{
            assert(mGameoverWindow_ != null);
            _gui.destroy(mGameoverWindow_);
            mGameoverWindow_ = null;
        }
    }

    function start(){

        mPauseButtonWindow_ = _gui.createWindow();
        mPauseButtonWindow_.setSize(200, 200);
        mPauseButtonWindow_.setVisualsEnabled(false);

        mPauseButton_ = mPauseButtonWindow_.createButton();
        mPauseButton_.setKeyboardNavigable(false);
        mPauseButton_.setSkinPack("PauseButton");
        mPauseButton_.setSize(65, 65);
        mPauseButton_.setText("");
        mPauseButton_.attachListenerForEvent(function(widget, action){
            showPauseMenu(true);
        }, 2,  this);

        mHealthBar_ = HealthBar(mPlayerHealth_);

        player_ = ::Player(SlotPosition(1, 2, 0));

        mEnemyManager.createEnemyWave(10);
    }

    function end(){
        _gui.destroy(mPauseButton_);
        _gui.destroy(mPauseButtonWindow_);
        if(mPauseWindow_) showPauseMenu(false);
        if(mGameoverWindow_) showGameoverMenu(false);
        mHealthBar_.destroy();

        foreach(i in enemies_){
            i.destroy();
        }
        foreach(i in projectiles_){
            i.destroy();
        }
        enemies_.clear();
        projectiles_.clear();

        player_.destroy();
    }

    function update(){
        if(gameplayPaused) return;

        movementCount_++;
        if(movementCount_ >= 100){
            movementDir_ = !movementDir_
            movementCount_ = 0;
        }

        local movement = Vec3(0.05, -mGameplaySpeed_, 0);
        movement.x *= movementDir_ ? 1 : -1;
        foreach(i in enemies_){
            i.move(movement);
            i.updateAnimations();
            if(i.getPosition().y < -10){
                destroyEnemy(i.getId());
                mPlayerHealth_--;
                mHealthBar_.setBarValue(mPlayerHealth_);
                if(mPlayerHealth_ <= 0){
                    showGameoverMenu(true);
                }
            }
        }
        movement = Vec3(0, 0.25, 0);
        foreach(i in projectiles_){
            i.move(movement);
            if(i.getPosition().y > 35){
                destroyProjectile(i.getId());
            }
        }
        player_.updateRotation();

        //Check to see if we need to create a new enemy wave.
        local highestEnemy = 0;
        local lowestEnemy = 5000;
        foreach(i in enemies_){
            local yPos = i.getPosition().y;
            if(yPos > highestEnemy){
                highestEnemy = yPos;
            }
            if(yPos < lowestEnemy){
                lowestEnemy = yPos;
            }
        }
        if(highestEnemy < 15 || enemies_.len() == 0 || highestEnemy - lowestEnemy < 5){
            mEnemyManager.createEnemyWave(24);
        }

        processInput();
    }

    function notifyTouchInputBegan(data){
        if(data.guiIntersect) return;
        const TOUCH_POS_CUTOFF = 0.75;

        local fingerId = data.fingerId;
        local pos = _input.getTouchPosition(data.fingerId);
        mTouchInputs_[fingerId] <- pos;
        if(mTouchControllingPlayer == null){
            if(pos.y > TOUCH_POS_CUTOFF){
                mTouchControllingPlayer = fingerId;
                print("Controlling touch began");
            }
        }
        if(pos.y <= TOUCH_POS_CUTOFF){
            createProjectile();
        }
    }
    function notifyTouchInputEnded(fingerId){
        mTouchInputs_.rawdelete(fingerId);
        if(mTouchControllingPlayer == fingerId){
            mTouchControllingPlayer = null;
            print("Controlling touch ended");
        }
    }
    function notifyTouchInputMotion(fingerId){
        //Note the events are queued, so this try catch has to be here incase a motion comes in after the touch has been removed.
        try{
            if(fingerId == mTouchControllingPlayer){
                local pos = _input.getTouchPosition(fingerId);

                local endPos = SlotPosition(0, 2, 0);
                endPos += Vec3((pos.x) * 12.0 - 5, 0, 0);
                player_.setPosition(endPos)
            }
        }catch(e){

        }

    }

    function processInput(){
        if(_settings.getUserSetting("touchControls")){

            if(_input.getMouseY() >= (_window.getHeight() / 4) * 3){
                local endPos = SlotPosition(0, 2, 0);
                endPos += Vec3((_input.getMouseX().tofloat() / _window.getWidth().tofloat()) * 12.0 - 5, 0, 0);
                player_.setPosition(endPos)
            }else{
                createProjectile();
            }

        }else{
            //Process input
            if(_input.getButtonAction(::InputShootHandle, _INPUT_PRESSED)){
                createProjectile();
            }
            local amount = Vec3(_input.getAxisActionX(::InputMoveHandle) * 0.1, 0, 0);
            player_.move(amount);
        }
    }

};