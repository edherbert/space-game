function inputTouchBegan(event, data){
    ::currentState.notifyTouchInputBegan(data);
}
function inputTouchEnded(event, data){
    ::currentState.notifyTouchInputEnded(data);
}
function inputTouchMotion(event, data){
    ::currentState.notifyTouchInputMotion(data);
}

_event.subscribe(_EVENT_SYSTEM_INPUT_TOUCH_BEGAN, inputTouchBegan);
_event.subscribe(_EVENT_SYSTEM_INPUT_TOUCH_ENDED, inputTouchEnded);
_event.subscribe(_EVENT_SYSTEM_INPUT_TOUCH_MOTION, inputTouchMotion);