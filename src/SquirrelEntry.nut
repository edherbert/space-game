enum Component{
    HEALTH = 0,
    GENERAL = 1,
    MISC = 2
}

function start(){
    ::buttonSize <- Vec2(350, 90);
    ::gameplayPaused <- false;

    {
        local renderTexture = _window.getRenderTexture();
        local __currentFinalWorkspace = _compositor.addWorkspace([renderTexture], _camera.getCamera(), "fullRes_workspace", true);
    }

    {
        local light = _scene.createLight();
        local lightNode = _scene.getRootSceneNode().createChildSceneNode();
        lightNode.attachObject(light);

        light.setType(_LIGHT_DIRECTIONAL);
        light.setDirection(-1, -1, -1);
        light.setPowerScale(PI);

        _scene.setAmbientLight(0xffffffff, 0xffffffff, Vec3(0, 1, 0));
    }

    _camera.setProjectionType(_PT_ORTHOGRAPHIC);
    _camera.setOrthoWindow(40, 40);
    _camera.setPosition(0, 0, 5);
    _camera.lookAt(0, 0, 0);
    _camera.setPosition(1, 15, 5);

    _gui.loadSkins("res://assets/skins/guiSkin.json");
    _animation.loadAnimationFile("res://assets/animations/shipAnimations.xml");

    _world.createWorld();

    local winSize = Vec2(_window.getWidth(), _window.getHeight())
    _gui.setCanvasSize(winSize, winSize);

    _input.setActionSets({
        "Gameplay" : {
            "Buttons" : {
                "Shoot": "#Shoot"
            },
            "StickPadGyro" : {
                "Move":"#Move"
            }
        }
    });
    ::InputShootHandle <- _input.getButtonActionHandle("Shoot");
    ::InputMoveHandle <- _input.getAxisActionHandle("Move");
    _input.mapControllerInput(_B_A, ::InputShootHandle);
    _input.mapKeyboardInput(_K_SPACE, ::InputShootHandle);
    _input.mapControllerInput(_BA_LEFT, ::InputMoveHandle);
    _input.mapKeyboardInputAxis(_K_D, _K_S, _K_A, _K_W, ::InputMoveHandle);

    _doFile("res://src/CallbackFunctions.nut");
    _doFile("res://src/HealthBar.nut");
    _doFile("res://src/GameStateMenu.nut");
    _doFile("res://src/GameStatePlaying.nut");
    _doFile("res://src/Entities.nut");
    _doFile("res://src/SharedState.nut");
    ::currentState <- null;

    ::sharedState <- SharedState();

    //Start the menu off.
    if(_settings.getUserSetting("skipStartupMenu")){
        startState(::GameStatePlaying);
    }else{
        startState(::GameStateMenu);
    }
}

function update(){
    ::currentState.update();
    if(!gameplayPaused) ::sharedState.update();
}

function end(){

}

::startState <- function(stateClass){
    if(::currentState != null){
        ::currentState.end();
    }
    ::currentState = stateClass();

    ::currentState.start();
}

::_applyDamage <- function(entity, damage){
    if(!entity.valid()) return;
    local newHealth = _component.user[Component.HEALTH].get(entity, 0) - damage;
    _component.user[Component.HEALTH].set(entity, 0, newHealth);
}