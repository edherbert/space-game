function projectile(id, type, internalId, sender, receiver){
    if(type != _COLLISION_ENTER) return;

    _applyDamage(receiver, 5);

    ::currentState.destroyProjectile(sender.getId());
}